#!/usr/bin/env python
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN


tourDescription = """

# srsLTE Controlled RF

This profile uses srsLTE in the controlled RF environment to create an LTE
network.
"""

tourInstructions = """

### Starting srsLTE

Startup scripts will install srsLTE. The following will run it.

Log in to enb and run:

```
sudo srsepc
```

Log in to enb and run:

```
sudo srsenb --gui.enable=1
```

Log in to ue and run:

```
sudo srsue --gui.enable=1
```

To run a traffic test use iperf.
Log in to the enb and run:

```
iperf -s -p 1234
```

Log in to the ue and run:

```
iperf -c 172.16.0.1 -p 1234
```
"""


class GLOBALS(object):
    UBUNTU_1804_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
    NUC_HWTYPE = "nuc5300"


pc = portal.Context()

request = pc.makeRequestRSpec()

# Add a NUC eNB node
enb = request.RawPC("enb")
enb.hardware_type = GLOBALS.NUC_HWTYPE
enb.disk_image = GLOBALS.UBUNTU_1804_IMG
enb.Desire("rf-controlled", 1)
enb_ue_rf = enb.addInterface("ue_rf")

# Add a NUC UE node
ue = request.RawPC("ue")
ue.hardware_type = GLOBALS.NUC_HWTYPE
ue.disk_image = GLOBALS.UBUNTU_1804_IMG
ue.Desire("rf-controlled", 1)
ue_enb_rf = ue.addInterface("enb_rf")

# Create the RF link between the UE and eNodeB
rflink = request.RFLink("rflink")
rflink.addInterface(enb_ue_rf)
rflink.addInterface(ue_enb_rf)

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

enb.addService(rspec.Execute(shell="bash",
                             command="/local/repository/startup.sh"))
ue.addService(rspec.Execute(shell="bash",
                            command="/local/repository/startup.sh"))


pc.printRequestRSpec(request)

