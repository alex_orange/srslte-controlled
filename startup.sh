#!/bin/bash

# Get the emulab repo 
wget -qO - http://repos.emulab.net/emulab.key | sudo apt-key add -
sudo add-apt-repository -y http://repos.emulab.net/powder/ubuntu/
sudo apt-get update

sudo DEBIAN_FRONTEND=noninteractive apt-get install -y uhd-host libuhd-dev

sudo "/usr/lib/uhd/utils/uhd_images_downloader.py"

sudo DEBIAN_FRONTEND=noninteractive apt-get install -y git

sudo DEBIAN_FRONTEND=noninteractive apt-get install -y cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev
sudo DEBIAN_FRONTEND=noninteractive apt-get install -y libboost-system-dev libboost-test-dev libboost-thread-dev libqwt-qt5-dev qtbase5-dev

cd /local

git clone https://github.com/srsLTE/srsGUI.git
cd srsGUI
mkdir build
cd build
cmake ../
make
sudo make install

cd /local
git clone https://github.com/srsLTE/srsLTE

cd srsLTE

patch cmake/modules/SRSLTE_install_configs.sh.in <<END
--- SRSLTE_install_configs.sh.in.orig	2020-10-20 09:49:31.442003326 -0600
+++ SRSLTE_install_configs.sh.in	2020-10-20 09:50:03.699068220 -0600
@@ -72,7 +72,7 @@
     # Set file ownership to user calling sudo
     if [ \$SUDO_USER ]; then
       user=\$SUDO_USER
-      chown \$user:\$user \$dest_path
+      chown \$user:\$SUDO_GID \$dest_path
     fi
   else
     echo " - \$source_path doesn't exists. Skipping it."
END

mkdir build
cd build
cmake ../
make

sudo make install
sudo srslte_install_configs.sh service

sudo ldconfig
